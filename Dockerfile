# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:latest

# Create a directory to store your application files
WORKDIR /usr/src/towerofhanoi

# Copy all files from your local towerofhanoi folder to the image's working directory
COPY . .

# Compile your application using g++
RUN g++ -o myapp proiect.cpp

# Run your application by default when the container starts
CMD ["./myapp"]

# Set metadata for the image
LABEL Name=towerofhanoi Version=0.0.1



