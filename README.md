   În realizarea proiectului sincretic, prin intermediul editorului Visual Studio Code, am utilizat următoarele extensii: _Code Runner_, _WSL_ și _Docker_.
   Extensia _Code Runner_ facilitează executarea programului pentru rezolvarea problemei Turnurilor din Hanoi, fără a fi necesară deschiderea unei console separate.
   Extensia _WSL_ permite lucrul cu proiectul propriu-zis într-un mediu Linux direct din editorul VS Code de pe Windows, simplifică procesul de dezvoltare, și oferă astfel o experiență de programare mai integrată.
   Extensia _Docker_ facilitează dezvoltarea și gestionarea containerelor Docker direct din cadrul editorului. Acesta oferă o serie de funcționalități precum: vizualizarea containerelor și imaginilor, editarea fișierelor Dockerfile, rularea și debugging-ul programelor etc.


   Pentru început, am adăugat un Docker File în Workspace, apoi am modificat comenzile din cadrului fișierului conform mediului nostru de lucru. Work directory-ul l-am setat pe calea folderului de lucru, iar pentru comanda _run_ care este folosită în compilarea aplicației am asociat fișierul sursă al proiectului.
   Pentru crearea imaginii Dockerului, am folosit comanda _docker build -t myapp ._ Imaginea Docker este un pachet folosit în scopul rulării unui program, inclusiv a codului și a timpului de rulare a bibliotecii. După crearea acesteia, Dockerul este pregătit pentru rulare și aceasta se realizează prin instrucțiunea _docker run myapp_.
   Git-ul este componenta folosită pentru gestionarea versiunilor în aplicație și care facilitează contribuția mai multor persoane în cadrul aceluiași proiect. Pentru acest proiect, am utilizat platforma GitLab. În Git există numeroase funcționalități pe care le folosim pentru a putea controla versiunile unui proiect: _git init_, _git add ._, _git commit_, _git push_, _git remote_, _git pull_ și _git clone_. Pe lângă acestea, în cadrul proiectului, pe platforma GitLab, am creat un repository pe care ulterior l-am clonat local pe Windows. 
   _Git init_ este o comandă folosită pentru inițializarea unui nou repository într-un director existent.
   _Git add ._ pregătește modificările pentru următoarea etapă, adică pentru commit. 
   _Git commit -m "mesaj asociat commit-ului"_ finalizează procesul de adăugare a schimbărilor în zona de pregătire și le salvează în repository alături de un mesaj sugestiv.
   _Git remote add origin_ este o comandă utilizată pentru a conecta remote la un repository.
   Prin _git push --set-upstream origin master_ (alternativ putem utiliza -u pentru --setupstream) salvăm modificările realizate local către repository-ul remote. _Origin master_ stabilește o legătură între branch-ul local curent și branch-ul remote. 
   La finalul parcurgerii acestor instrucțiuni, se pot vedea modificările pe platforma GitLab.

   
   Turnurile din Hanoi este un joc matematic care este format din trei tije și un număr variabil de discuri, de diferite mărimi, care pot fi poziționate pe oricare dintre cele trei tije. Jocul începe având discurile așezate în stivă pe prima tijă, în ordinea mărimii lor, astfel încât să formeze un turn. Scopul jocului este acela de a muta întreaga stivă, de pe o tijă pe cealaltă, respectând următoarele reguli: 
  - doar un singur disc poate fi mutat, la un moment dat;
  - fiecare mutare constă în luarea celui de mai sus disc de pe o tijă și glisarea lui pe o altă tijă, chiar și deasupra altor discuri care sunt deja prezente pe acea tijă;
  - un disc mai mare NU poate fi poziționat deasupra unui disc mai mic.
   Particularizând, cu 3 discuri jocul se rezolvă în 7 mișcări. Pe caz general, numărul minim de mișcări pentru a rezolva jocul este 2^n-1, n reprezentând numărul de discuri utilizate.


   Am rezolvat problema Turnurilor din Hanoi în limbajul de programare C++ respectând regulile stabilite anterior, soluția implicând utilizarea recursivității pentru a muta discurile de la un turn la altul.
   În cadrul programului, funcția _main_ inițializează variabilele pentru tijele A, B și C (tija_initiala, tija_intermediara și tija_finala) și n (numărul de discuri), apoi am apelat funcția _hanoi_ care primește 4 argumente: n (numărul de discuri), sursa (tija sursă), intermediar (tija intermediară) și destinatie (tija destinație). În cazul de bază, când avem un singur disc (n == 1), se afișează mutarea discului de pe tija sursă la tija destinație. În caz contrar, se aplică următorul algoritm recursiv:
  - mută n-1 discuri de pe tija sursă la tija intermediară, folosind tija destinație ca tijă intermediară;
  - mută discul n de la tija sursă la tija destinație;
  - mută din nou n-1 discuri de pe tija intermediară la tija destinație, folosind tija sursă ca tijă intermediară.
