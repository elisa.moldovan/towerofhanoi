#include <iostream>

void hanoi(int n, char sursa, char intermediar, char destinatie) {
    if (n == 1) {
        std::cout << "Muta discul 1 de la " << sursa << " la " << destinatie << std::endl;
        return;
    }

    // muta n-1 discuri de pe tija sursa pe tija intermediara, folosind tija destinatie ca intermediara
    hanoi(n - 1, sursa, destinatie, intermediar);

    // muta discul n de la tija sursa la tija destinatie
    std::cout << "Muta discul " << n << " de la " << sursa << " la " << destinatie << std::endl;

    // muta, din nou, n-1 discuri de pe tija intermediara la tija destinatie folosind tija sursa ca intermediara
    hanoi(n - 1, intermediar, sursa, destinatie);
}

int main() 
{
    int n = 4;
    //std::cout << "Introduceti numarul de discuri: ";
    //std::cin >> n;
    char tija_initiala = 'A';
    char tija_intermediara = 'B';
    char tija_finala = 'C';

    // apelare functie
    hanoi(n, tija_initiala, tija_intermediara, tija_finala);
    return 0;
}
